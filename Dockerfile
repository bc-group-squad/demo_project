FROM openjdk:11-jre-slim
EXPOSE 8090
VOLUME /tmp
ENV JAR_FILE=build/libs/demo_project-0.0.1-SNAPSHOT.jar
#can be ARG
COPY $JAR_FILE app.jar
#can be ADD
ENTRYPOINT ["java", "-jar", "/app.jar"]
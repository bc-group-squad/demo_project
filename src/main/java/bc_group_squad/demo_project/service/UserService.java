package bc_group_squad.demo_project.service;

import bc_group_squad.demo_project.domain.dto.UserDTO;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Long createUser(UserDTO userDTO);

    List<UserDTO> getUsers();

    Optional<UserDTO> getUserByID(Long userId);

    Optional<UserDTO> updateUser(Long userId, UserDTO userDTO);

    void deleteUser(Long userId);
}

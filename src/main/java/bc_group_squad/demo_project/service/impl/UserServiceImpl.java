package bc_group_squad.demo_project.service.impl;

import bc_group_squad.demo_project.domain.dto.UserDTO;
import bc_group_squad.demo_project.domain.entity.User;
import bc_group_squad.demo_project.domain.exception.ObjectNotFoundException;
import bc_group_squad.demo_project.repository.UserRepository;
import bc_group_squad.demo_project.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ObjectMapper objectMapper;

    @Override
    public Long createUser(UserDTO userDTO) {
        User savedUser = userRepository.save(objectMapper.convertValue(userDTO, User.class));
        return savedUser.getId();
    }

    @Override
    public List<UserDTO> getUsers() {
        return userRepository.findAll().stream()
                .map(UserDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<UserDTO> getUserByID(Long userId) {
        return userRepository.findById(userId)
                .map(UserDTO::new);
    }

    @Override
    public Optional<UserDTO> updateUser(Long userId, UserDTO userDTO) {
        return userRepository.findById(userId)
                .map(user -> user.update(userDTO))
                .map(userRepository::save)
                .map(UserDTO::new);
    }

    @Override
    public void deleteUser(Long userId) {
        try {
            userRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException(userId, User.class);
        }
    }
}

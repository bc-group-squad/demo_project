package bc_group_squad.demo_project.controller;

import bc_group_squad.demo_project.domain.dto.UserDTO;
import bc_group_squad.demo_project.domain.entity.User;
import bc_group_squad.demo_project.domain.exception.ObjectNotFoundException;
import bc_group_squad.demo_project.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/users")
    public ResponseEntity<Long> createUser(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.createUser(userDTO));
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserDTO> getUserByID(@PathVariable(value = "id") Long userId) {
        UserDTO userGetById = userService.getUserByID(userId)
                .orElseThrow(() -> new ObjectNotFoundException(userId, User.class));
        return ResponseEntity.ok(userGetById);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserDTO> updateUser(@PathVariable(value = "id") Long userId, @RequestBody UserDTO userDTO) {
        UserDTO updatedUser = userService.updateUser(userId, userDTO)
                .orElseThrow(() -> new ObjectNotFoundException(userId, User.class));
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable(value = "id") Long userId) {
        userService.deleteUser(userId);
        return ResponseEntity.ok().build();
    }
}

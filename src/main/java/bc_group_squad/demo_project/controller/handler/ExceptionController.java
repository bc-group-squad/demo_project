package bc_group_squad.demo_project.controller.handler;

import bc_group_squad.demo_project.domain.exception.NotFoundExceptionsGroup;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {NotFoundExceptionsGroup.class})
    public ResponseEntity<Object> handleNotFoundExceptionsGroup(NotFoundExceptionsGroup ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}

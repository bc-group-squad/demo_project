package bc_group_squad.demo_project.repository;

import bc_group_squad.demo_project.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}

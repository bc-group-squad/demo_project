package bc_group_squad.demo_project.domain.entity;

import bc_group_squad.demo_project.domain.dto.UserDTO;
import bc_group_squad.demo_project.domain.enums.RoleUser;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
@RequiredArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email", unique = true, nullable = false)
    private String email;
    @Column(name = "login", unique = true, nullable = false)
    private String login;
    @Column(name = "password")
    private String password;
    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private RoleUser role = RoleUser.USER;

    public User update(UserDTO userDTO) {
        this.firstName = userDTO.getFirstName();
        this.lastName = userDTO.getLastName();
        this.email = userDTO.getEmail();
        this.login = userDTO.getLogin();
        this.password = userDTO.getPassword();
        this.role = userDTO.getRole();
        return this;
    }
}

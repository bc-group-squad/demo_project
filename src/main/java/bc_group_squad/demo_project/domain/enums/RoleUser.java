package bc_group_squad.demo_project.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RoleUser {
    ADMIN("ADMIN"),
    USER("USER");

    private final String value;

    RoleUser(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static RoleUser fromValue(String value) {
        for (RoleUser b : RoleUser.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

}

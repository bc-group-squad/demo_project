package bc_group_squad.demo_project.domain.exception;

public class ObjectNotFoundException extends NotFoundExceptionsGroup {
    public ObjectNotFoundException(Long objectId, Class<?> className) {
        super(className.getSimpleName() + " with id: " + objectId + " has not been found");
    }
}

package bc_group_squad.demo_project.domain.exception;

public class NotFoundExceptionsGroup extends RuntimeException {
    public NotFoundExceptionsGroup(String message) {
        super(message);
    }
}

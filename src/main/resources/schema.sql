DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    id         BIGINT       NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id),
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    email      VARCHAR(255) NOT NULL UNIQUE,
    login      VARCHAR(255) NOT NULL UNIQUE,
    password   VARCHAR(255) NOT NULL,
    role       VARCHAR(10)
);
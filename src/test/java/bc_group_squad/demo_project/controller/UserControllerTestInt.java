package bc_group_squad.demo_project.controller;

import bc_group_squad.demo_project.DemoProjectApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = DemoProjectApplication.class)
@AutoConfigureMockMvc
public class UserControllerTestInt {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void userShouldReturnIdAfterCreate() throws Exception {
        String postBody = "{\"firstName\" : \"firstName\"," +
                            "\"lastName\" : \"lastName\"," +
                            "\"email\" : \"email\"," +
                            "\"login\" : \"login\"," +
                            "\"password\" : \"password\"," +
                            "\"role\" : \"USER\"}";

        var result = mockMvc.perform(post("/users").content(postBody)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("4", result.getResponse().getContentAsString(StandardCharsets.UTF_8));
    }
}
